package com.example.alex.weather;

import android.app.Application;
import android.content.Context;

import com.facebook.stetho.Stetho;

/**
 * App class for getting context
 */
public class App extends Application {

    private static Context context;

    public static Context getAppContext() {
        return App.context;
    }

    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Stetho.initializeWithDefaults(this);
        }
        App.context = getApplicationContext();
    }
}